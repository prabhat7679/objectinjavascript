const mapObj=require('../mapObject.cjs');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

 const callbackFn = value =>{
    if( typeof value === 'string'){
        return (value+' '+'welcome!');
    }else{
        return (value+value);
    }
 }

 const finalObject=mapObj(testObject,callbackFn);
 console.log(finalObject);