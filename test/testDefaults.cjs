const defaultFn=require("../defaults.cjs");
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// taking other default object
const defObject={
    name: 'Bruce Wayne',
    age: 36, 
    location: 'Gotham',
    state :'India',
    pin : 724520,
}
 // answer object
const finalObject=defaultFn(testObject, defObject);

console.log(finalObject);