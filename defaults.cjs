function defaultFn(testObject, defObject){
    for(let key in defObject){  
        if(testObject[key]=== undefined){
            testObject[key]= defObject[key];
        }
    }
    return testObject;
}
module.exports=defaultFn;