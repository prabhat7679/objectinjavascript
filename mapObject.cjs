function mapObj(testObject,callback){
    for(let key in testObject){
        testObject[key] = callback(testObject[key]);
    }
    return testObject;
}
module.exports=mapObj;